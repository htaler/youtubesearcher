using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using newapp.Models;

namespace newapp.Controllers
{
    public class MoviesController : Controller
    {
        // GET: /<controller>/
        public IActionResult Index(string nextPageToken)
        {
            return View(RetrieveMoviesFromYoutube(nextPageToken));
        }

        private YouTubeMovieData RetrieveMoviesFromYoutube(string nextPageToken)
        {
            string search = "cały film";
            IYouTubeReader reader = new YouTubeReader();
            reader.SetNextPageToken(nextPageToken);
            reader.SetSearchTerm(search);
            reader.SetKey("AIzaSyD91zDCXzpirhgLsw3OcOTeQvYhAY1uLGk");
            reader.GetJson();
            YouTubeMovieData result = reader.GetDeserializedJson();
            return result;
        }
    }
}