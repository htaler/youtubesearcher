﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using newapp.Models;

namespace newapp
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            // app.Use(async (context, next) =>
            // {
            //     string html = MovieList();

            //     await context.Response.WriteAsync(html);
            //     await next();
            // });
            app.UseStaticFiles();
            app.UseMvc(routes => 
            {
                routes.MapRoute("Default", "{controller=Movies}/{action=Index}/{search?}");
            });
        }

        private string MovieList()
        {
            MovieListBuilder movieListBuilder = new MovieListBuilder();

            string html = "<!doctype html><html lang=\"en\"><head><meta charset=\"utf-8\"><title>The HTML5 Herald</title><body>";

            var result = RetrieveMoviesFromYoutube();



            if (result != null)
            {
                html = html + CreateMoviesHTML(result);
            }
            else
            {
                html = html + "not retrieved any movies";
            }

            html = html + "</body></html>";
            return html;
        }

        private string CreateMoviesHTML(YouTubeMovieData movieData)
        {
            string html = string.Empty;
            if (movieData != null && movieData.items.Count > 0)
            {
                foreach (var item in movieData.items)
                {
                    string url = GenerateYoutubeUrl(item.id.videoId);
                    html = html + $"<b><a href=\"{url}\">{item.snippet.title}</a></b><br>{item.snippet.description}<br>";
                    html = html + $"<img src=\"{item.snippet.thumbnails.High.url}\"><br><br>";
                }
            }
            return html;
        }

        private YouTubeMovieData RetrieveMoviesFromYoutube()
        {
            string search = "cały film";
            IYouTubeReader reader = new YouTubeReader();
            reader.SetSearchTerm(search);
            reader.SetKey("AIzaSyDCV3t0ghErfl3iB5yK8MJRjpF0fJJvA5Q");
            reader.GetJson();
            YouTubeMovieData result = reader.GetDeserializedJson();
            return result;
        }

        private string GenerateYoutubeUrl(string videoId)
        {
            return $"https://www.youtube.com/watch?v={videoId}";
        }
    }
}
