﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace newapp.Models
{
    public class YouTubeMovieData
    {
        public string kind { get; set; }
        public string nextPageToken {get;set;}
        public List<YouTubeMovieDataDetails> items { get; set; }
    }

    public class YouTubeMovieDataDetails
    {
        public YouTubeVideoId id { get; set; }

        public string YoutubeUrl
        {
            get {  return $"https://www.youtube.com/watch?v={id.videoId}";}
            
        }
        public YouTubeVideoSnippet snippet { get; set; }
    }

    public class YouTubeVideoSnippet
    {
        public string title { get; set; }
        public string description { get; set; }

        public YouTubeVideoThumbnails thumbnails {get;set;}
    }

    public class YouTubeVideoThumbnails
    {
        public YouTubeVideoThumbnailData Default {get;set;}
        public YouTubeVideoThumbnailData Medium {get;set;}
        public YouTubeVideoThumbnailData High {get;set;}
    }

    public class YouTubeVideoThumbnailData
    {
        public string url {get;set;}
        public int width {get;set;}
        public int height {get;set;}
    }

    public class YouTubeVideoId
    {
        public string kind { get; set; }
        public string videoId { get; set; }
    }

    



}
