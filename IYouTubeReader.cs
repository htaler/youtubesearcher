﻿using System;
using System.Collections.Generic;
using System.Text;
using newapp.Models;

namespace newapp
{
    public interface IYouTubeReader
    {
        void SetSearchTerm(string searchTerm);
        void SetNextPageToken(string SetNextPageToken);
        string SearchTerm { get; }
        void SetKey(string key);
        string GetJson();
        YouTubeMovieData GetDeserializedJson();
    }
}
