using System;
using System.Collections;
using System.Collections.Generic;

namespace newapp
{
    public class MovieListBuilder
    {

        public List<MovieData> GetMovieList()
        {
            List<MovieData> result = new List<MovieData>();

            // some fake results
            result.Add(new MovieData{VideoUrl="http://youtube.com", Title="Miami Vice", ImageUrl="http://youtube.com/image.jpg", Description="fajny film o policjantach"});
            return result;

        }
    }
}