﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using newapp.Models;

namespace newapp
{
    // example request

    // https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=50&type=video&q=cały%20film&key=AIzaSyCY54RLXm6VAX8p-tbaq_POrxfoDM1PWLU
    public class YouTubeReader : IYouTubeReader
    {
        private string _searchTerm;

        private string _nextPageToken;
        private string _jSon;
        private string _key;

        public string SearchTerm
        {
            get
            {
                return _searchTerm;
            }
        }

        public void SetNextPageToken(string token)
        {
            _nextPageToken = token;
        }

        private string BuildSearchUrl()
        {
            if (_nextPageToken == null)
            {
                return $"https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=50&type=video&q={_searchTerm}&key={_key}";
            }
            else
            {
                return $"https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=50&type=video&pageToken={_nextPageToken}&q={_searchTerm}&key={_key}";
            }
        }

        public YouTubeMovieData GetDeserializedJson()
        {
            YouTubeMovieData data = JsonConvert.DeserializeObject<YouTubeMovieData>(_jSon);
            return data;
        }

        public string GetJson()
        {
            WebClient webClient = new WebClient();
            _jSon = webClient.DownloadString(BuildSearchUrl());
            return _jSon;
        }

        public void SetKey(string key)
        {
            _key = key;
        }

        public void SetSearchTerm(string searchTerm)
        {
            _searchTerm = searchTerm;
        }
    }
}
